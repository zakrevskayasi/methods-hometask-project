package com.stormnet.methods;

public class Task6 {
    static int cube(int a) {
        return a * a * a;
    }

    public static void main(String[] args) {
        System.out.println(cube(2));
    }
}
