package com.stormnet.methods;

import java.util.Scanner;

public class ComparatorTask3 {
    private static Scanner scanner = new Scanner(System.in);

    static int minNumberPrint(int a, int b) {
        if (a > b) {
            return b;
        } else {
            return a;
        }
    }

    public static void main(String[] args) {
        System.out.println("Please enter the first number: ");
        int num1 = scanner.nextInt();
        System.out.println("Please enter the second number: ");
        int num2 = scanner.nextInt();

        System.out.println(minNumberPrint(num1, num2));
    }
}
