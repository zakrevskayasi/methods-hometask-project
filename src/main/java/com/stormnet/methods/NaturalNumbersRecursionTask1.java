package com.stormnet.methods;

import java.util.Scanner;

public class NaturalNumbersRecursionTask1 {
    public static String getNaturalNumbers(int n){
        if(n == 1){
            return "1";
        }else {
            return getNaturalNumbers(n - 1) + " " +  n;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter n: ");
        System.out.println(getNaturalNumbers(scanner.nextInt()));
    }
}
